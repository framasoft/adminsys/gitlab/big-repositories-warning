#!/usr/bin/env perl
use strict;
use warnings;
use Mojo::Base -base;
use Mojo::File;
use Mojo::JSON qw(decode_json);
use GitLab::API::v4;
use Number::Bytes::Human qw(format_bytes parse_bytes);
use Email::Valid;
binmode(STDOUT, ":utf8");

my $config = decode_json(Mojo::File->new('config.json')->slurp) || {};

die "You have to set Gitlab URL in config.json!"            unless (defined($config->{gitlab_url}));
die "You have to set an API token in config.json!"          unless (defined($config->{secret_token}));
die "You have to set a maximum size in config.json!"        unless (defined($config->{max_size}));
die "You have to set a valid email address in config.json!" unless (defined($config->{email}) && Email::Valid->address($config->{email}));

my $max_size = parse_bytes($config->{max_size}) or die "Unable to parse the maximum size. Please see documentation on https://metacpan.org/pod/Number::Bytes::Human.";
my @big_repos;

my $api = GitLab::API::v4->new(
    url           => $config->{gitlab_url},
    private_token => $config->{secret_token}
);

say 'Fetching projects statistics…';
my $i = 1;
$|++; # unbuffered output
my $paginator = $api->paginator('projects', {statistics => 1});
while (my $project = $paginator->next()) {
    print "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b".$i++;
    push @big_repos, {
        id              => $project->{id},
        url             => $project->{http_url_to_repo},
        name            => $project->{name},
        owner           => $project->{owner},
        namespace_id    => $project->{namespace}->{id},
        repository_size => $project->{statistics}->{repository_size}
    } if $project->{statistics}->{repository_size} >= $max_size;
}
say ' repositories checked';
$|--;
say sprintf '%d repositories over %s', scalar(@big_repos), format_bytes($max_size);

@big_repos = sort { $b->{repository_size} <=> $a->{repository_size} } @big_repos;

for my $big_project (@big_repos) {
    say sprintf('%s (id: %s): %s', $big_project->{name}, $big_project->{id}, format_bytes($big_project->{repository_size}));
    use Data::Dumper;
    if (defined $big_project->{owner}->{id}) {
        my $email = $api->user($big_project->{owner}->{id})->{email};
        say sprintf('  owner: %s <%s> (id: %d)', $big_project->{owner}->{name}, $email, $big_project->{owner}->{id}) if defined $big_project->{owner};
    } else {
        my @maintainers;
        my $members_paginator = $api->paginator('project_members', $big_project->{id});
        my $members = 0;
        while (my $member = $members_paginator->next()) {
            if ($member->{access_level} == 50) {
                my $email = $api->user($member->{id})->{email};
                say sprintf('  owner: %s <%s> (id: %d)', $member->{name}, $email, $member->{id});
            }
            push @maintainers, $member if $member->{access_level} == 40;
            $members++;
        }
        $members_paginator = $api->paginator('group_members', $big_project->{namespace_id});
        while (my $member = $members_paginator->next()) {
            if ($member->{access_level} == 50) {
                my $email = $api->user($member->{id})->{email};
                say sprintf('  owner: %s <%s> (id: %d)', $member->{name}, $email, $member->{id});
            }
            push @maintainers, $member if $member->{access_level} == 40;
        }
        for my $maintainer (@maintainers) {
            my $email = $api->user($maintainer->{id})->{email};
            say sprintf('  maintainer: %s <%s> (id: %d)', $maintainer->{name}, $email, $maintainer->{id});
        }
    }
}
